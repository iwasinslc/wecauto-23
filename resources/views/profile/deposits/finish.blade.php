@extends('layouts.profile')
@section('title', __('Car order'))
@section('content')
    <section class="order-step">
        <div class="container">
            <div class="car-orders">
                <h3 class="lk-title">{{__('Car order')}}
                </h3>
            </div>
            <ul class="steps-indicator">
                <li class="is-active"> <span>1</span>
                </li>
                <li class="is-active"> <span>2</span>
                </li>
                <li class="is-active"> <span>3</span>
                </li>
                <li class="is-active is-current"> <span>4</span>
                </li>
            </ul>
            <div class="steps-title">
                <h4><span class="color-warning">{{__('Step')}}</span> #4</h4><small>{{__('Activate payment')}}</small>
            </div>
            <div class="steps-activate">
                <div class="steps-activate__row">
                    <div class="steps-activate__content">
                        <div class="steps-intro">
                            <p>{{__('Activate the auto program.')}}</p>
                            <p>{{__('Pay Down Payment')}} 35%</p>
                        </div>
                        <div class="contribution">
                            <p class="contribution__subtitle">* {{__('Early withdrawal - impossible!')}}
                            </p>
                            <div class="contribution__row">
                                <div class="contribution__col">
                                    <div class="contribution-block">
                                        <p class="contribution-block__title">{{__('Your contribution')}}:
                                        </p>
                                        <p class="contribution-block__count">{{$data['amount']}} <small>WEC</small>
                                        </p>
                                        <p class="contribution-block__desc">{{__('Activate the auto program.')}} {{__('Pay Down Payment')}} 35%
                                        </p>
                                    </div>
                                </div>
                                <div class="contribution__col">
                                    <div class="pay-now">
                                        <p class="pay-now__count">35%
{{--                                        </p><a class="pay-now__btn" href="#">Pay now!</a>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="steps-activate__image">
                        <div class="steps-activate__car-image"><img src="{{getPhotoPath(isset($data['image']) ? $data['image'] : '')}}" alt="">
                        </div>
                    </div>
                </div>
                <div class="add-fst">
                    <div class="vehicle-item__top">
                        <p class="order-card__title"><strong>{{$data['name']}}</strong></p>
                        <div class="clock-action">
                            <div class="clock-action__top">
                                <svg class="svg-icon">
                                    <use href="/assets/icons/sprite.svg#icon-wall-clock"></use>
                                </svg>
                                <p>12m {{__('left')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="v-indicator">
                        <div class="v-indicator__bar"><span style="width:0%"><span>{{now()->format('d/m')}} <span>‘{{now()->format('y')}}</span></span></span>
                        </div>
{{--                        <p class="v-indicator__start-date">22/02 <span>‘20</span>--}}
{{--                        </p>--}}
                        <p class="v-indicator__end-date">{{now()->addMonths(12)->format('d/m')}}  <span>‘{{now()->addMonths(12)->format('y')}}</span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="terms scrollbar-inner">
                <div class="terms__content">
                    <h3 class="terms__title">{{__('Terms and agreement')}}
                    </h3>
                    <div class="typography">
<ul>
    <li>Внесение первоначального взноса по автопрограмме WECAUTO производится исключительно в монетах WEC (Web Coin Pay), в размере 30% от общей стоимости транспортного средства в долларовом эквиваленте.</li>
    <li>Участие в программе автокредитования допускается только после приобретения лицензии, условия и стоимость которых обозначены в маркетинг-плане WECAUTO.</li>
    <li>Каждая лицензия имеет свой ограниченный срок действия. После окончания срока действия лицензии для дальнейшего участия в автопрограмме пользователю необходимо приобрести новую лицензию.</li>
    <li>Согласно маркетинг-плану программы WECAUTO, по истечении срока вклада участнику начисляется 100% от стоимости транспортного средства в коинах WEC. Данная сумма зачисляется единовременно и одним платежом сразу после окончания срока действия тарифного плана.</li>
    <li>Начисления по стандартному автодепозиту производятся после истечения 12 месяцев со дня внесения первоначального взноса. Пользователь имеет возможность сократить действие своего депозита путём приобретения фастеров FST ―растущего цифрового актива. После каждой активации FST дата окончания действия вклада смещается в сторону приближения, точную информацию о чём можно получить в личном кабинете пользователя.</li>
    <li>Участник имеет право распоряжаться фастерами на своё усмотрение, покупая или продавая их на внутренней P2P-бирже в любом необходимом объёме.</li>
    <li>Пользователь вправе претендовать на получение FST на бесплатной основе, в виде кэшбека от приобретения лицензии, за участие в партнёрской программе или в качестве bounty-вознаграждения за выполнение определённых целевых действий.</li>
    <li>Курс обмена WEC/USD и в обратном направлении не зависит от администрации компании, а формируется на внутренней P2P за счёт пользовательской активности.</li>
    <li>Участник имеет право оформлять неограниченное количество взносов на приобретение транспортных средств на базе одного личного кабинета.</li>
    <li>Условия сотрудничества администрации и пользователей ресурса предусматривают невозможность досрочного прекращения срока действия автопрограммы, а также досрочного возврата и вывода первоначального взноса.</li>
    <li>Пользователь данного ресурса получает доступ к партнёрской программе WECAUTO и может привлекать других лиц к сотрудничеству, за что будет получать партнёрские начисления в процентном соотношении от сумм депозитов рефералов. Размеры начислений прописаны в условиях партнёрской программы.</li>
    <li>Участнику может быть отказано в выплате в случае, если он допускает нарушения правил сотрудничества, в частности, если он использует собственную реферальную ссылку для создания аккаунтов-клонов в целях получения финансовой выгоды в рамках партнёрской программы.</li>
    <li>Участник автопрограммы WECAUTO может претендовать на получение каждого из рангов партнёрской программы при условии выполнения требований, предъявляемых к тому или иному рангу.</li>
</ul>
                    </div>
                </div>
            </div>
            <div class="order-step__buttons">
                <form method="post" enctype="multipart/form-data" action="{{route('profile.deposits.finish_handle')}}">
                    {{csrf_field()}}
                    <button class="btn btn--warning btn--size-md">{{__('Complete order')}}
                    </button>
                    <label class="checkbox">
                        <input name="agreement" type="checkbox"><span>{{__('Accept Agreement')}}</span>
                    </label>
                </form>

            </div>
        </div>
    </section>
    <!-- /.card -->
@endsection

@push('load-scripts')

@endpush