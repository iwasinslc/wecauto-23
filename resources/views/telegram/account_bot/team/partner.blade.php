@if($partner!==null&&$partner->my_id!=$telegramUser->user->my_id)
<?php
$partnerTelegram = $partner->telegramUser()->first();
?>
{{__('Name')}} - {{$partner->name}}
E-mail - {{$partner->email}}
{{__('Telegram')}} - {{$partnerTelegram? '@'.$partnerTelegram->username : __('None')}}
@else
{{__('None')}}
@endif