<?php
return [
    'emails' => [
        'spam_protection'   => 'A carta não é enviada. Regras do spam.',
        'sent_successfully' => 'A carta é enviada com êxito',
        'unable_to_send'    => 'É impossível enviar esta carta',

        'request' => [
            'email_required' => 'Email é obrigatório',
            'email_max'      => 'O comprimento máximo de email é de 255 símbolos',
            'email_email'    => 'Email incorrecto!',

            'text_required' => 'Texto é obrigatório',
            'text_min'      => 'O comprimento mínimo de texto é de 10 símbolos',
        ],
    ],
    'transaction_types' => [
        'enter'      => 'Recargo do saldo',
        'withdraw'   => 'Retiro dos fundos',
        'bonus'      => 'Bonificação',
        'partner'    => 'Comissão de sócio',
        'dividend'   => 'Ganância do depósito',
        'create_dep' => 'Criação do depósito',
        'close_dep'  => 'Fechamento do depósito',
        'penalty'    => 'Multa',
    ],

];
