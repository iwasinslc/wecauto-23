<?php
namespace App\Http\Controllers\Payment;

use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Modules\PaymentSystems\CoinpaymentsModule;
use App\Modules\PaymentSystems\CryptoCurrencyApiModule;
use App\Modules\PaymentSystems\EtherApiModule;
use App\Modules\PaymentSystems\WebCoinApiModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class EtherApiController
 * @package App\Http\Controllers\Payment
 */
class CryptoCurrencyApiController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function topUp()
    {
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = session('topup.payment_system');

        /** @var Currency $currency */
        $currency = session('topup.currency');

        if (empty($paymentSystem) || empty($currency)) {
            return redirect()->route('profile.topup')->with('error', __('Can not process your request, try again.'));
        }

        $amount = abs(session('topup.amount'));

        $amount = round($amount, 6);
$user = user();
        $wallet = $user->getUserWallet($currency->code, $paymentSystem->code);
        \Log::critical(print_r($wallet,true));
        if (empty($wallet)) {
            $wallet = Wallet::newWallet($user, $currency, $paymentSystem);
        }

        if (null === $wallet->address) {
            $address = CryptoCurrencyApiModule::getAddress($currency->code);

            $wallet->address = $address;
            $wallet->save();
            $wallet->fresh();
        }




        return view('ps.cryptocurrency', [
            'currency' => $currency->name,
            'amount' => $amount,
            'statusUrl' => route('perfectmoney.status'),
            'user' => $user,
            'wallet' => $wallet,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function status(Request $request)
    {





        $rawRequest = json_decode(file_get_contents('php://input'), true);
        if ($rawRequest === FALSE || empty($rawRequest)|| !isset($rawRequest['cryptocurrencyapi.net'])) {
            \Log::critical('CCApi. Strange request from: '.$request->ip().', Error reading POST data. '.print_r($request->all(),true));
            return response('ok');
        }



        $sign = sha1(implode(':', array(
            $rawRequest['currency'],
            $rawRequest['type'],
            $rawRequest['date'],
            $rawRequest['address'],
//            $rawRequest['from'],


            $rawRequest['amount'],
            $rawRequest['txid'],
            $rawRequest['pos'],
            $rawRequest['confirmations'],
            $rawRequest['tag'],
            env('CC_API_KEY') // токен доступа к API
        )));

        if ($sign !== $rawRequest['sign2']) {
            \Log::critical('EtherApi. Strange request from: '.$request->ip().', HMAC signature does not match. '.print_r($request->all(),true));
            return response('ok');
        }

        if (!$request->has('amount') ||

            !$request->has('txid') ||
            !$request->has('type') ||
            $request->type!='in' ||
            !$request->has('tag')) {
            \Log::info('CryptoCurrencyApiModule. Strange request from: '.$request->ip().'. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }

        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = PaymentSystem::where('code', 'cryptocurencyapi')->first();
        /** @var Currency $currency */
        $currency      = Currency::where('code', $request->currency)->first();

        if (null == $currency) {
            \Log::critical('Strange request from: '.$request->ip().'. Currency not found. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }

        /** @var Wallet $wallet */
        $wallet = Wallet::where('currency_id', $currency->id)
            ->where('payment_system_id', $paymentSystem->id)
            ->where('address', $request->address)
            ->first();

        if ($wallet===null)
        {
            \Log::critical('Strange request from: '.$request->ip().'. User not found. Entire request is: '.print_r($request->all(),true));
        }


        /** @var User $user */
        $user = $wallet->user;


        /** @var Transaction $transaction */
        $transaction = Transaction::where('currency_id', $currency->id)
            ->where('payment_system_id', $paymentSystem->id)
            ->where('wallet_id', $wallet->id)
            ->where('batch_id', $rawRequest['txid'])
            ->orderBy('created_at', 'desc')
            ->limit(1)
            ->first();
        \Log::critical(print_r($transaction,true));
        if ($request->confirmations >= 2&&$transaction===null)
        {
            $transaction = Transaction::enter($wallet, $request->amount);

            if (null!==$transaction)
            {

                $transaction->batch_id = $request->txid;
                $transaction->result = 'complete';
                $transaction->source = $request->address;
                $transaction->save();
                $commission = $transaction->amount * 0.01 * $transaction->commission;

                //$wallet->refill(($transaction->amount-$commission), $transaction->source);
                $transaction->update(['approved' => true]);


                $amount = ($transaction->amount - $commission)*rate($currency->code, 'USD');

                /** @var Wallet $wallet */
                $wallet = $user->getUserWallet('USD', 'perfectmoney');


                $exchange = Transaction::enter_exchange($wallet, $amount, Currency::getByCode('USD'), PaymentSystem::getByCode('perfectmoney'));

                $wallet->refill($amount);

                NotificationEvent::dispatch($user, 'notifications.wallet_refiled', [
                    //'id'=>$user->id,
                    'user_id'=>$user->id,
                    'amount'=>$exchange->amount,
                    'currency'=>$exchange->currency->code
                ]);




                CryptoCurrencyApiModule::getBalances(); // обновляем баланс нашего внешнего кошелька в БД
                return response('OK');

            }
        }



        \Log::emergency('CryptoCurrencyApiModule transaction is not passed. IP: '.$request->ip().'. '.print_r($request->all(), true));
        return response('OK');
    }
}
