<?php
namespace App\Http\Controllers\Payment;


use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Modules\PaymentSystems\CustomApiModule;
use App\Modules\PaymentSystems\WebCoinApiModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class CustomApiController
 * @package App\Http\Controllers\Payment
 */
class CustomApiController extends Controller
{
//    /**
//     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
//     */
    public function topUp()
    {
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = session('topup.payment_system');

        /** @var Currency $currency */
        $currency = session('topup.currency');

        if (empty($paymentSystem) || empty($currency)) {
            return redirect()->route('profile.topup')->with('error', __('Can not process your request, try again.'));
        }

        $amount = abs(session('topup.amount'));
        $user          = Auth::user();

        $wallet = getUserWallet($currency->code, $paymentSystem->code);

        if ($wallet===null) {
            $wallet = Wallet::newWallet($user, $currency, $paymentSystem);
        }

        if (null == $wallet->address) {
            $address = CustomApiModule::getAddress();

            $wallet->address = $address;
            $wallet->save();
            $wallet->fresh();
        }




        return view('ps.custom', [
            'currency' => $currency->code,
            'amount' => $amount,
            'statusUrl' => route('perfectmoney.status'),
            'user' => $user,
            'wallet' => $wallet,
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function status(Request $request)
    {


        \Log::critical(print_r($request->all(), 1));

        $key = env('ACC_API_KEY');

        if ($request->api_key!=$key)
        {

            throw new \Exception('Wrong Api Key');
        }


        $check = Transaction::where('batch_id', $request->tx_id)->first();

        if ($check!==null)
        {
            return response()->json(['result'=>'ok', 'txid'=>$check->id], 200);
        }

        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = PaymentSystem::where('code', 'custom')->first();
        /** @var Currency $currency */
        $currency      = Currency::where('code', strtoupper($request->currency))->first();
        if (!$request->has('address')||empty($request->address))
        {
            throw new \Exception('User not found.');
        }



        /** @var Wallet $wallet */
        $wallet = Wallet::where('currency_id', $currency->id)
            ->where('payment_system_id', $paymentSystem->id)
            ->where('address', $request->address)
            ->first();

        if ($wallet===null)
        {

            \Log::critical($request->address);

            \Log::info('Strange request from: '.$request->ip().'. User not found. Entire request is: '.print_r($request->all(),true));


            throw new \Exception('User not found.');
        }


        try {


            /** @var User $user */
            $user = $wallet->user;

            $transaction  = Transaction::enter($wallet,$request->amount);

            $transaction->update(['approved' => true, 'batch_id'=>$request->tx_id]);

            $amount = (float)$request->amount;



            if ($transaction!==null)
            {
                $wallet->addAmountWithoutAccrueToPartner($amount);
            }


        }
        catch (\Exception $e)
        {
            throw new \Exception('Wallet not found.');
        }







        $user->sendTelegramNotification('balance_updated', [
            'transaction' => $transaction,
        ]);


        return response()->json(['result'=>'ok'], 200);


    }
}
