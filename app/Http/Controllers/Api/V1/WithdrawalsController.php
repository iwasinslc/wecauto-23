<?php


namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RequestDataTable;
use App\Http\Resources\WithdrawalResource;
use App\Models\Withdraw;

class WithdrawalsController extends Controller
{
    /**
     * @OA\Get(
     *      path="/withdrawals",
     *      operationId="getWithdrawals",
     *      tags={"Withdrawals"},
     *      summary="Get list of the withdrawals",
     *      description="Return array of the withdrawals. With filter: user_id",
     *      @OA\Parameter(
     *          ref="#/components/parameters/user_id"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/WithdrawalResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index(RequestDataTable $request) {
        return WithdrawalResource::collection(
                Withdraw::with(['paymentSystem', 'status', 'currency'])
                    ->where('user_id', $request->user_id)->get()
            );
    }
}