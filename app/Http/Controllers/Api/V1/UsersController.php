<?php


namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RequestUserUpdate;
use App\Http\Requests\Api\RequestUserSearch;
use App\Http\Resources\UserSearchResource;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class UsersController extends Controller
{

    /**
     * @OA\Get(
     *      path="/users/search",
     *      operationId="searchUser",
     *      tags={"Users"},
     *      summary="Get user information",
     *      description="Returns user data",
     *      @OA\Parameter(
     *          name="login",
     *          description="User's login or email",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="wallet_address",
     *          description="The address of one of a user's wallets",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserSearchResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function search(RequestUserSearch $request)
    {
        $query = User::with([
            'wallets',
            'licence',
            'partner',
            'rank'
        ]);

        if ($request->filled('login')) {
            // Search by login or email
            $query = $query->where(\DB::raw('LOWER(login)'), strtolower($request->query('login')))
                ->orWhere(\DB::raw('LOWER(email)'), strtolower($request->query('login')));
        } elseif ($request->filled('wallet_address')) {
            // Search by wallet address
            $query = $query->whereHas('wallets', function ($hasMany) use ($request) {
                $hasMany->where('address', $request->query('wallet_address'));
            });
        } elseif ($request->filled('batch_id')) {
            // Search by batch id
            $transactionType = TransactionType::getByName('enter');
            if (!$transactionType) {
                throw new \Exception('Transaction type enter not found');
            }
            $transaction = Transaction::query()
                ->where('type_id', $transactionType->id)
                ->where('batch_id', $request->query('batch_id'))
                ->first();
            if (!$transaction) {
                throw new \Exception('Transaction not found');
            }
            $query = $query->where('id', $transaction->user_id);
        } elseif ($request->filled('wallet_id')) {
            // Search by wallet id
            $query = $query->whereHas('wallets', function ($hasMany) use ($request) {
                $hasMany->where('id', $request->query('wallet_id'));
            });
        } elseif ($request->filled('customer_id')) {
            $query->where('id', $request->customer_id);
        } else {
            throw new \Exception('Not available search type');
        }

        $user = $query->first();

        abort_if(empty($user), Response::HTTP_NOT_FOUND, 'User not found');

        return new UserSearchResource($user);
    }

    /**
     * @OA\Put(
     *      path="/users/{id}",
     *      operationId="updateUser",
     *      tags={"Users"},
     *      summary="Update existing user",
     *      description="Update users data data",
     *      @OA\Parameter(
     *          name="id",
     *          description="User id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/RequestUserUpdate")
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful updated",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="User Not Found"
     *      )
     * )
     */
    public function update(RequestUserUpdate $request, string $id)
    {
        $requestData = $request->validated();

        abort_if(empty($requestData), Response::HTTP_BAD_REQUEST, __('validation.request.bad'));

        /** @var User $user */
        $user = User::find($id);

        abort_if(empty($user), Response::HTTP_NOT_FOUND, __('validation.user.not_found'));

        if(isset($requestData['google_2fa']) && $user->isGoogle2FaEnabled()) {
            // Disable google 2fa if parameter google_2fa present
            $user->setGoogle2FaDisabled();
            unset($requestData['google_2fa']);
        }
        if(isset($requestData['email']) && $requestData['email'] != $user->email && $user->isVerifiedEmail()) {
            // Reset email verification
            $requestData['email_verified_at'] = null;
        }

        if(!empty($requestData['password'])) {
            $requestData['password'] = \Hash::make($requestData['password']);
        }

        if(empty($requestData) || $user->update($requestData)) {
            return response()->json(__('validation.user.updated'), Response::HTTP_ACCEPTED);
        } else {
            return response()->json(__('validation.user.not_updated'), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}