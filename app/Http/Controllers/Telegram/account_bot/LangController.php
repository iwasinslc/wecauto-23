<?php
namespace App\Http\Controllers\Telegram\account_bot;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\account_bot\Auth\IndexController;
use App\Http\Controllers\Telegram\account_bot\Settings\PersonalData\LoginController;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;

use App\Modules\Messangers\TelegramModule;

class LangController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @throws \Throwable
     */
    public function index(
        TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event
    ) {
        sleep(1);
        $scope = TelegramBotScopes::where('method_address', 'like', '%LangController%')
            ->where('bot_keyword', $bot->keyword)
            ->first();

        TelegramModule::setLanguageLocale($telegramUser->language);

        preg_match('/set\_lang ([a-z]+)/', $event->text, $data);



        $user = $telegramUser->user;

        if (null == $user) {
            return response('ok');
        }



        if (isset($data[1])) {
            \Log::info($data[1]);
            $telegramUser->language = $data[1];
            $telegramUser->save();
            $message = view('telegram.account_bot.settings.lang.success', [
                'webhook'      => $webhook,
                'bot'          => $bot,
                'scope'        => $scope,
                'telegramUser' => $telegramUser,
                'event'        => $event,
            ])->render();

            try {
                $telegramInstance = new TelegramModule($bot->keyword);
                $telegramInstance->sendMessage(
                    $event->chat_id,
                    $message,
                    'HTML',
                    true,
                    false,
                    null,
                    null,
                    $scope,
                    'inline_keyboard'
                );
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response('ok');
            }

            if (password_verify('password', $user->password)) {


                app()->call(IndexController::class.'@index', [
                    'webhook' => $webhook,
                    'bot' => $bot,
                    'scope' => $scope,
                    'telegramUser' => $telegramUser,
                    'event' => $event,
                ]);
                return response('ok');
            }
            app()->call(StartController::class.'@index', [
                'webhook' => $webhook,
                'bot' => $bot,
                'scope' => $scope,
                'telegramUser' => $telegramUser,
                'event' => $event,
            ]);
            return response('ok');
        }



        $message = view('telegram.account_bot.settings.lang.lang', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        $keyboard = [
            [
                ['text' => '🏴󠁧󠁢󠁥󠁮󠁧󠁿 English', 'callback_data' => 'set_lang en'],
                ['text' => '🇷🇺 Russian', 'callback_data' => 'set_lang ru'],
            ],
        ];

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage(
                $event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                [
                    'inline_keyboard' => $keyboard,
                    'resize_keyboard' => true,
                ],
                $scope,
                'inline_keyboard'
            );
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        return response('ok');
    }



}
