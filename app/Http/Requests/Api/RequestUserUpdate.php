<?php

namespace App\Http\Requests\Api;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @OA\Schema(
 *      title="RequestUserUpdate",
 *      description="Update user request body data",
 *      type="object",
 *      required={},
 *      @OA\Property(property="email",ref="#/components/schemas/User/properties/email"),
 *      @OA\Property(
 *          title="Google 2FA",
 *          property="google_2fa",
 *          description="Parameter to disable google 2fa. If it present. Google 2fa will be disabled.",
 *          example=false,
 *          type="boolean"
 *      )
 * )
 */
class RequestUserUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Security are testing in middleware
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param $user
     * @return array
     */
    public function rules(User $user)
    {
        return [
            'email' => [
                'email',
                'max:150',
                Rule::unique($user->getTable(), 'email')->ignore(request()->route('user')),
            ],
            'login'      => [
                'string', 'max:30',
                Rule::unique($user->getTable(), 'login')->ignore(request()->route('user')),
            ],
            'password'   => 'sometimes|required|string|max:255',
            'is_blocked' => 'boolean',
            'google_2fa' => 'boolean',
        ];
    }
}
