<?php

namespace App\Events;

use App\Models\Notification;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NotificationEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $broadcastQueue = 'default';

    /** @var array $data */
    public $data;

    /**
     * MyBalanceUpdated constructor.
     * @param Wallet $wallet
     */
    public function __construct( User $user, $message ,$data)
    {


        /** @var array balanceData */
        $this->data = $data;
        $this->data['user_id'] = $user->id;

        /**
         * @var Notification $notification
         */
        $notification = Notification::add($user, $message, $data);

        $this->data['message'] = $notification->translate($user);

        $this->data['id'] = $notification->id;

        $this->data['type'] = $notification->type;

        $this->data['created_at'] = Carbon::parse($notification->created_at)->format('d-m-Y H:i:s') ;



    }



    public function broadcastOn()
    {
        $ownerId = $this->data['user_id'];
        return new PrivateChannel('user.'.$ownerId);
    }

}
