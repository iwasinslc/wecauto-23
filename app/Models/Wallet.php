<?php

namespace App\Models;

use App\Events\MyBalanceUpdated;
use App\Events\NotificationEvent;
use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Wallet
 * @package App\Models
 *
 * @property string id
 * @property string user_id
 * @property string currency_id
 * @property string external
 * @property string payment_system_id
 * @property float balance
 * @property string address
 * @property User user
 * @property Currency currency
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Wallet extends Model
{
    use ModelTrait;
    use Uuids;

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    /** @var array $fillable */
    protected $fillable = [
        'user_id',
        'currency_id',
        'payment_system_id',
        'external',
        'balance',
        'address'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentSystem()
    {
        return $this->belongsTo(PaymentSystem::class, 'payment_system_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'wallet_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deposits()
    {
        return $this->hasMany(Deposit::class, 'wallet_id');
    }

    /**
     * @param $value
     * @return float
     * @throws \Exception
     */
    public function getBalanceAttribute($value)
    {
        if (null == $value) {
            $value = 0;
        }

        return currencyPrecision($this->currency_id, $value);
    }

    /**
     * @return float|null
     * @throws \Exception
     */
    public function requestedAmount()
    {
        $sum = Withdraw::where('wallet_id', $this->id)
            ->where('status_id', TransactionStatus::STATUS_CONFIRMED_BY_EMAIL)
            ->sum('amount');

        $sum = $sum ? $this->getBalanceAttribute($sum) : null;
        return $sum;
    }

    /**
     * @param float $amount
     * @param array $transactionData
     * @return $this
     */
    public function addBonus($amount, array $transactionData = []) : Wallet
    {
        $commission = TransactionType::getByName('bonus')->commission;
        Transaction::bonus($this, $amount, $transactionData);
        $this->update(['balance' => $this->balance + $amount - $amount * $commission * 0.01]);
        //$this->user->checkFiveFactor();
        return $this;
    }

    /**
     * @param $amount
     * @return $this
     */
    public function removeAmount($amount)
    {
        $this->update(['balance' => $this->balance - $amount]);
        $this->refresh();
        MyBalanceUpdated::dispatch($this);
        return $this;
    }

    /**
     * @param $amount
     * @param string $type
     * @param $approved
     * @return $this
     */

    public function makePenalty($amount, $type='', $approved = false)
    {
        Transaction::penalty($this, $amount, $type, $approved);
        if($approved) {
            $this->approvePenalty($amount);
        }
        return $this;
    }

    /**
     * Make penalty after approving
     * @param $amount
     */
    public function approvePenalty($amount) {
        $this->update(['balance' => $this->balance - $amount]);
    }

//    /**
//     * @param $amount
//     * @return $this
//     */
//    public function penaltyAmount($amount)
//    {
//        Transaction::penalty($this, $amount);
//        $this->update(['balance' => $this->balance - $amount]);
//        $this->refresh();
//        MyBalanceUpdated::dispatch($this);
//        return $this;
//    }

    /**
     * @param float $amount
     * @return bool
     */
    public function addAmountWithoutAccrueToPartner($amount = 0.00)
    {
        $this->update(['balance' => $this->balance + $amount]);
        $this->refresh();
        MyBalanceUpdated::dispatch($this);

        return true;
    }

    /**
     * @param float $amount
     * @param string $partnerAccrueType
     * @return int
     * @throws \Throwable
     */
    public function addAmountWithAccrueToPartner($amount = 0.00, $partnerAccrueType = 'deposit')
    {
        $this->update(['balance' => $this->balance + $amount]);
        $this->refresh();
        MyBalanceUpdated::dispatch($this);
        return $this->accrueToPartner($amount, $partnerAccrueType);
    }

    /**
     * @param $user
     * @param $currency
     * @param $paymentSystem
     * @return mixed
     */
    public static function newWallet($user, $currency, $paymentSystem)
    {
        $checkExists = Wallet::where('user_id', $user->id)
            ->where('currency_id', $currency->id)
            ->where('payment_system_id', $paymentSystem->id)
            ->first();

        if (null !== $checkExists) {
            return $checkExists;
        }

        return self::create([
            'user_id' => $user->id,
            'currency_id' => $currency->id,
            'payment_system_id' => $paymentSystem->id,
        ]);

    }

    /**
     * @param $amount
     * @param $external
     * @throws \Throwable
     */
    public function refill($amount, $external = '')
    {

        $this->refresh();
        $this->balance += $amount;

        $this->save();

        MyBalanceUpdated::dispatch($this);

    }

    /**
     * @param Withdraw $transaction
     */
    public function returnFromRejectedWithdrawal(Withdraw $transaction)
    {
        $this->update(['balance' => $this->balance + $transaction->amount*rate($transaction->currency->code, $this->currency->code)]);
        $this->refresh();
        MyBalanceUpdated::dispatch($this);
    }

    /**
     * @param $amount
     * @param $type
     * @return int
     * @throws \Throwable
     */
    public function accrueToPartner($amount, $type = 'deposit')
    {
        /** @var User $user */
        $user = $this->user;
        $partnerLevels = $user->getPartnerLevels();

        if (!$partnerLevels) {
            return 0;
        }

        foreach ($partnerLevels as $level) {
            /** @var User $partner */
            $partner = $user->getPartnerOnLevel($level);

            if (empty($partner)) {
                \Log::critical('haventPartner');
                continue;
            }


            $percent = Referral::getOnLoad($level);

            $rank = $partner->rank;

            $licence = $partner->licence;


            if ($licence === null) {
                \Log::critical('haventLicence');
                continue;
            }

            if (!$partner->activeLicence()) {
                \Log::critical('haventLicence');
                continue;
            }


            if ($level > $rank->levels) {
                \Log::critical('rankll');
                continue;
            }



            if ($percent <= 0) {
                \Log::critical('pecc');
                continue;
            }


            $partnerAmount = $amount * $percent / 100;

            if ($user->licence->price > $licence->price) {
                $partnerAmount = $licence->price * rate('USD', 'FST') * $percent * 0.01;

            }


            $partnerWallets = $partner->wallets()
                ->where('currency_id', $this->currency_id)
                ->get();

            if ($partnerWallets->count() == 0) {
                /** @var Wallet $newPartnerWallet */
                $newPartnerWallet = self::newWallet($partner, $this->currency, null);
                $newPartnerWallet->referralRefill($partnerAmount, $this, $type, $level);
            }

            $summaryAmount = 0;

            /** @var Wallet $partnerWallet */
            foreach ($partnerWallets as $partnerWallet) {
                if ($partnerWallet->currency == $this->currency) {
                    $partnerWallet->referralRefill($partnerAmount, $this, $type, $level);
                    $summaryAmount += $partnerAmount;


//                    $notificationActive = $partner->socialMeta()
//                        ->where('s_key', 'settings_notifications_referral ' . $level . '_level')
//                        ->where('s_value', 1)
//                        ->count();
//
//                    if ($notificationActive > 0) {
//                        $partner->sendNotification('affiliate_earnings', [
//                            'amount' => $partnerAmount,
//                            'receiveWallet' => $partnerWallet,
//                            'sender' => $user,
//                            'receive' => $partner,
//                            'level' => $level,
//                        ]);
//                    }

                    break;
                }
            }
        }
        return isset($summaryAmount) ? $summaryAmount : 0;
    }


    public function accrueToPartnerLicence($amount, $type, $transaction)
    {
        /** @var User $user */
        $user = $this->user;
        $partnerLevels = $user->getPartnerLevels();

        if (!$partnerLevels) {
            return 0;
        }

        foreach ($partnerLevels as $level) {


            if ($level > 1) {
                \Log::critical('rankll');
                continue;
            }


            /** @var User $partner */
            $partner = $user->getPartnerOnLevel($level);

            if (empty($partner)) {
                \Log::critical('haventPartner');
                continue;
            }


            $percent = Referral::getOnLoad($level);

            //$rank = $partner->rank;

            $licence = $partner->licence;


            if ($licence === null) {
                \Log::critical('haventLicence');
                continue;
            }

            if (!$partner->activeLicence()) {
                \Log::critical('haventLicence');
                continue;
            }

            $parse = $partner->buy_at;

            if ($parse>$transaction->created_at)
            {
                continue;
            }

            if ($percent <= 0) {
                \Log::critical('pecc');
                continue;
            }


            $partnerAmount = $amount * $percent / 100;

            if ($user->licence->price > $licence->price) {
                $partnerAmount = $licence->price * rate('USD', 'FST') * $percent * 0.01;

            }


            $partnerWallets = $partner->wallets()
                ->where('currency_id', $this->currency_id)
                ->get();

            if ($partnerWallets->count() == 0) {
                /** @var Wallet $newPartnerWallet */
                $newPartnerWallet = self::newWallet($partner, $this->currency, null);
                $newPartnerWallet->referralRefill($partnerAmount, $this, $type, $level);
            }

            $summaryAmount = 0;

            /** @var Wallet $partnerWallet */
            foreach ($partnerWallets as $partnerWallet) {
                if ($partnerWallet->currency == $this->currency) {
                    $partnerWallet->referralRefill($partnerAmount, $this, $type, $level);
                    $summaryAmount += $partnerAmount;


//                    $notificationActive = $partner->socialMeta()
//                        ->where('s_key', 'settings_notifications_referral ' . $level . '_level')
//                        ->where('s_value', 1)
//                        ->count();
//
//                    if ($notificationActive > 0) {
//                        $partner->sendNotification('affiliate_earnings', [
//                            'amount' => $partnerAmount,
//                            'receiveWallet' => $partnerWallet,
//                            'sender' => $user,
//                            'receive' => $partner,
//                            'level' => $level,
//                        ]);
//                    }

                    break;
                }
            }
        }
        return isset($summaryAmount) ? $summaryAmount : 0;
    }

    /**
     * @param $amount
     * @param $referral
     * @param $type
     * @param int $level
     */
    public function referralRefill($amount, $referral, $type, $level = 0)
    {

        /**
         * @var User $refUser
         */
        $refUser = $this->user;

//        if ($refUser !== null) {
//            \Log::error('refback user found ' . $refUser->id);
//
//            if ($refUser->refback > 0) {
//                $refback = (float)$refUser->refback;
//                $refbackAmount = $amount / 100 * $refback;
//                $amount -= $refbackAmount;
//
//                $referral->refill($refbackAmount, '', true);
//                Transaction::refback($referral, $refbackAmount);
//
//                \Log::error('found refback ' . $refUser->refback . ', amount ' . $refbackAmount);
//            }
//        }

        if ($amount > 0) {

            $this->update(['balance' => $this->balance + $amount]);

            if ($type == 'refill') {
                Transaction::partner($this, $amount, $referral, $level);


                //Transaction::reserve_acc($this, $reserved_amount, $referral, $level);
            } elseif ($type == 'deposit') {
                Transaction::partner($this, $amount, $referral, $level);
                //Transaction::reserve_acc($this, $reserved_amount, $referral, $level);
            } elseif ($type == 'task') {
                Transaction::partner($this, $amount, $referral, $level);
                //Transaction::reserve_acc($this, $reserved_amount, $referral, $level);
            }

//            $data = [
//                'refill_amount' => $amount,
//                'balance' => $this->balance,
//                'currency' => $this->currency,
//                'type' => $type
//            ];

            NotificationEvent::dispatch($refUser, 'notifications.partner_accrue', [
                //'id'=>$user->id,
                'user_id'=>$refUser->id,
                'amount'=>$amount,
                'currency'=>$this->currency->code,
                'login'=>$referral->user->login,
                'level'=>$level
            ]);

            //$this->user->sendNotification('partner_accrue', $data);
        }
    }

    /**
     * @param User $user
     */
    public static function registerWallets(User $user)
    {
        $paymentSystems = PaymentSystem::with([
            'currencies'
        ])->get();

        foreach ($paymentSystems as $paymentSystem) {
            foreach ($paymentSystem->currencies as $currency) {
                $checkExists = $user->wallets()
                    ->where('currency_id', $currency->id)
                    ->where('payment_system_id', $paymentSystem->id)
                    ->get()
                    ->count();

                if ($checkExists > 0) {
                    continue;
                }

                self::newWallet($user, $currency, $paymentSystem);
            }
        }
    }
}
