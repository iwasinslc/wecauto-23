<?php

namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Withdraw
 * @package App\Models
 * @property string id
 * @property string user_id
 * @property string wallet_id
 * @property string payment_system_id
 * @property float amount
 * @property string source - кошелек реферала пользователя, если это партнерская транзакция.
 * @property string result - ответ платежной системы.
 * @property string batch_id - ИД операции в платежной системе.
 * @property int $status_id
 * @property TransactionStatus status
 * @property float commission
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property string confirmation_code
 * @property Carbon confirmation_sent_at
 */
class Withdraw extends Model
{
    use HasFactory;
    use ModelTrait;
    use Uuids;

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    protected $dates = [
        'confirmation_sent_at'
    ];

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rank()
    {
        return $this->belongsTo(UserRank::class, 'source', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentSystem()
    {
        return $this->belongsTo(PaymentSystem::class, 'payment_system_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(TransactionStatus::class, 'status_id', 'id');
    }

    /**
     * @param $value
     * @return float
     * @throws \Exception
     */
    public function getAmountAttribute($value)
    {
        if (null == $this->currency_id) {
            return $value;
        }

        return currencyPrecision($this->currency_id, $value);
    }

    public function isApproved()
    {
        return $this->status_id == TransactionStatus::STATUS_APPROVED;
    }

    public function isRejected()
    {
        return $this->status_id == TransactionStatus::STATUS_REJECTED;
    }

}
