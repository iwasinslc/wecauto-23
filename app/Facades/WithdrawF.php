<?php


namespace App\Facades;


use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Withdraw;
use App\Services\WithdrawService;
use Illuminate\Support\Facades\Facade;

/**
 * Class WithdrawF
 * @method static create(User $user, Wallet $sender_wallet, float $amount, PaymentSystem $paymentSystem,Currency $currency, $external, $with_confirmation = true)
 * @method static confirm(Transaction[] $transactions)
 * @method static confirmByCode(string $confirmation_code)
 * @method static approve(Withdraw $transaction)
 * @method static reject(integer $transaction_id, User $owner = null)
 * @method static createWithdrawSwap(Wallet $wallet, float $amount)
 *
 * @package App\Facades
 */
class WithdrawF extends Facade
{
    protected static function getFacadeAccessor()
    {
        return WithdrawService::class;
    }
}