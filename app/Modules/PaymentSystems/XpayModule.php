<?php
namespace App\Modules\PaymentSystems;

use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\Withdraw;

/**
 * Class XpayModule
 * @package App\Modules\PaymentSystems
 */
class XpayModule
{
    /**
     * @return array
     */
    public function getBalances(): array
    {
        $ps       = PaymentSystem::getByCode('xpay');
        $balances = [];

        foreach ($ps->currencies as $currency) {
            $balances[$currency->code] = 0;
        }

        $ps->update([
            'external_balances' => json_encode($balances),
            'connected' => false,
        ]);

        return $balances;
    }

    /**
     * @param string $currency
     * @return float
     */
    public function getBalance(string $currency): float
    {
        $balances = self::getBalances();
        return key_exists($currency, $balances) ? $balances[$currency] : 0;
    }

    /**
     * @param Withdraw $transaction
     * @return mixed
     * @throws \Exception
     */
    public function transfer(Withdraw $transaction) {
        throw new \Exception('Enpay do not support API transfers. Please, handle this operation manually.');
    }
}