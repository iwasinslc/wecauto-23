<?php

namespace App\Console\Commands\Manual;

use App\Models\Currency;
use App\Models\Licences;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Console\Command;

class CreateTestUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:test-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create test user from test-users.csv file';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $licence = Licences::query()->orderBy('price', 'desc')->first();
        $lines = file(__DIR__ . '/test-users.csv');
        \DB::beginTransaction();
        try {
            $row = 0;
            foreach ($lines as $line) {
                $row++;
                $userData = explode(',', trim($line));
                $myId = generateMyId();

                $user = User::where('email', $userData[1])->first();

                if (!$user) {
                    $user = \App\Models\User::create([
                        'name' => $userData[0],
                        'email' => $userData[1],
                        'login' => $userData[0],
                        'password' => bcrypt($userData[2]),
                        'my_id' => $myId,
                        'partner_id' => $userData[3],
                        'phone' => $userData[4],
                    ]);
                }
                $this->info('row - ' . $row);
                $this->info($user->email);
                /** @var PaymentSystem $paymentSystem */
                foreach (PaymentSystem::get() as $paymentSystem) {
                    /** @var Currency $currency */
                    foreach ($paymentSystem->currencies()->get() as $currency) {
                        $checkExistsWallet = $user->wallets()
                            ->where('payment_system_id', $paymentSystem->id)
                            ->where('currency_id', $currency->id)
                            ->count();

                        if (0 == $checkExistsWallet) {
                            $wallet = Wallet::newWallet($user, $currency, $paymentSystem);
                            $wallet->balance = 10000000;
                            $wallet->save();
                        }

                        $wallet = Wallet::where('user_id', $user->id)
                            ->where('currency_id', $currency->id)
                            ->where('payment_system_id', $paymentSystem->id)
                            ->first();

                        if ($wallet && $wallet->balance < 10000000) {
                            $wallet->balance = 10000000;
                            $wallet->save();
                        }
                    }
                }

                if (!$user->licence_id) {
                    $user->licence_id = $licence->id;
                    $user->close_at = now()->addDays($licence->duration);

                    $user->sell_limit = $licence->sell_amount;
                    $user->buy_limit = $licence->buy_amount;
                    $user->save();
//                Transaction::buy_license($wallet, 50000, $licence->id);
                }
                \DB::commit();
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }

        return 0;
    }
}
